#include <iostream>
#include <iomanip>
#include <cmath>
#include <limits>
using namespace std;

int main(){

    int d = numeric_limits<double>::digits10;

    int n = 2;
    double s_a = sqrt(2) / 2;
    double s_b = sqrt(2) / 2;
    double epsilon = numeric_limits<double>::epsilon();

    cout << endl;

    while( s_a*s_a > epsilon ){

        cout    << "n = " << setw(2) << n
                << endl;
        cout    << "A"
                << " | s = " << scientific << s_a
                << " | u = " << fixed << setprecision(d) << ldexp(s_a,n)
                << " | U = " << fixed << setprecision(d) << ldexp(s_a,n) / sqrt(1 - s_a*s_a)
                << endl;
        cout    << "B"
                << " | s = " << scientific << s_b
                << " | u = " << fixed << setprecision(d) << ldexp(s_b,n)
                << " | U = " << fixed << setprecision(d) << ldexp(s_b,n) / sqrt(1 - s_b*s_b)
                << endl << endl;

        s_a = sqrt( (1 - sqrt(1 - s_a*s_a)) / 2 );
        s_b /= sqrt(2 * (1+sqrt( 1 - s_b*s_b )) );
        n++;
    }

}