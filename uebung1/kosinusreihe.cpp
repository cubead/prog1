#include <iostream>
#include <cmath>
using namespace std;

double factorial(int n){
    double f = 1;
    for(int i = 2; i <= n; i++ ){
        f *= i;
    }
    return f;
}

double kosinusreihe(double x){

    if( x < 0 ){
        x *= -1;
    }
    x -= (floor(x / (2 * M_PI))) * M_PI * 2;
    if( ( M_PI <= x ) && ( x < 2 * M_PI ) ){
        return -kosinusreihe(x - M_PI);
    }
    if( ( M_PI / 2 <= x ) && ( x < M_PI ) ){
        return -kosinusreihe(M_PI - x);
    }
    if( ( M_PI / 4 < x ) && ( x <= M_PI / 2 ) ){
        return sqrt( 1 - pow(kosinusreihe(M_PI / 2 - x), 2 ) );
    }


    double epsilon = 1e-15;
    double s = 1;
    int n = 0;

    while( abs( pow(x,2*n) / factorial(2*n) ) > epsilon * abs( s ) ){
        n ++;
        s += pow(-1,n) * pow(x,2*n) / factorial(2*n);
    }

    return s;
}


int main(){

    double x;
    cout << endl;
    cout << "x = ";
    cin >> x;
    cout << endl << endl;

    cout << "x = " << setprecision(4) << x << endl;
    cout << "cos(x) ~ " << setprecision(15) << kosinusreihe(x) << endl;
    cout << "cos(x) = " << setprecision(15) << cos(x) << endl << endl;

}