#include <iostream>
#include <iomanip>
#include <cmath>
#include <limits>
using namespace std;

int main(){

    double a_0, b_0;
    cout << "a_0 b_0: ";
    cin >> a_0 >> b_0;

    double epsilon = numeric_limits<double>::epsilon();

    int n = 0;
    double a_old = a_0, b_old = b_0;
    double c_old = a_0 * a_0 - b_0 * b_0;
    double a_new, b_new, c_new;
    double summand = c_old/2;
    double sum = summand;

    while( summand >= epsilon * a_0*a_0 ){
        a_new = (a_old + b_old)/2;
        b_new = sqrt(a_old * b_old);
        c_new = (c_old * c_old)/(16 * a_new * a_new);
        a_old = a_new;
        b_old = b_new;
        c_old = c_new;
        n++;
        summand = ldexp(c_old,n-1);
        sum += summand;
    }
    double U_a = 2 * M_PI * (a_0 * a_0 - sum) / a_old;

    double e = sqrt(1 - (b_0*b_0)/(a_0*a_0));
    a_old = 1;
    b_old = 2;
    n = 1;
    summand = e*e / 4;
    sum = summand;
    while(summand > epsilon){
        a_new = a_old * (2*n + 1);
        b_new = b_old * (2*n + 2);
        a_old = a_new;
        b_old = b_new;
        n++;
        summand = (a_old * a_old * pow(e,2*n)) / (b_old * b_old * (2*n -1));
        sum += summand;
    }
    double U_b = 2*M_PI*a_0*(1-sum);


    cout    << "a_0 = " << fixed << setprecision(4) << a_0 << endl
            << "b_0 = " << fixed << setprecision(4) << b_0 << endl
            << "U_a = " << scientific << setprecision(15) << U_a << endl
            << "U_b = " << scientific << setprecision(15) << U_b << endl;
}